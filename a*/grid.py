from enum import Enum

class tile(Enum):
    EMPTY = 1
    BLOCKED = 2
    QUERRYPOINT = 3
    PATH = 4
def makeGrid():
    gridWidth = 10;
    gridHeight = 10;
    gridData = []
    for i in range(gridHeight): #rows
        c = []
        for j in range(gridWidth): #columns
            c.append(tile.EMPTY)
        gridData.append(c)
    return gridData
