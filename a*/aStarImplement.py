from youreAStar import tile
def aStar(grid, start, stop):
    newGrid = []
    for row in grid:
        r = []
        for tile in row:
            r.append((tile, False))
        newGrid.append(r)
    grid = newGrid
    queue = []
    queue.append(grid[start[0]][start[1]])
    xMax = len(grid)
    yMax = len(grid[0])

    while queue:
        point = queue.pop(0)
        if point == stop:
            return True

        x = point[0]
        y = point[1]
        print(f"x{x}")
        print(f"y{y}")

        neighbors = []
        for xDif in range(-1,2):
            newX = x + xDif
            if(newX > 0 and newX < xMax):
                for yDif in range(-1,2):
                    newY = y + yDif
                    if(newY > 0 and newY < yMax):
                        #print(grid[newX][newY])
                        if(grid[newX][newY][1] == False):
                            queue.append((newX,newY))
                            print("here")
                            grid[newX][newY][1] = (x,y)
                            input()
                        else:
                            pass

    return False
