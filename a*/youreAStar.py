from graphics import *
from aStarImplement import *
from grid import tile, makeGrid
def drawGrid(gridData, win):
    for i in range(len(gridData)):
        for j in range(len(gridData[0])):
            llCorner = Point(i,j)
            uRCorner = Point(i+1,j+1)
            r = Rectangle(llCorner, uRCorner)
            if(gridData[i][j] == tile.EMPTY):
                r.setFill("white")
            if(gridData[i][j] == tile.BLOCKED):
                r.setFill("red")
            if(gridData[i][j] == tile.QUERRYPOINT):
                r.setFill("blue")
            if(gridData[i][j] == tile.PATH):
                r.setFill("GREEN")
            r.draw(win)
    update(30)





if __name__ == "__main__":
    height = 480
    width = 640
    win = GraphWin("A*", width, height, autoflush = False)
    win.setCoords(0,-1,10,10)

    gridData = makeGrid()
    drawGrid(gridData, win)

    exit = False
    mode = tile.BLOCKED
    t = None
    querryPoints = []
    while(not exit):
        click = win.checkMouse()
        key = win.checkKey()
        if(click != None):
            x = int(click.x)
            y = int(click.y)
            gridData[x][y] = mode
            if(mode == tile.QUERRYPOINT):
                querryPoints.append((x,y))
                if(len(querryPoints) > 2):
                    emptyPoint = querryPoints.pop(0)
                    print(emptyPoint)
                    gridData[emptyPoint[0]][emptyPoint[1]] = tile.EMPTY


        if(key == "q"): exit = True
        if(key == "w"):
            if mode == tile.BLOCKED:
                mode = tile.QUERRYPOINT
            elif mode == tile.QUERRYPOINT:
                mode = tile.BLOCKED
            print(mode)
        drawGrid(gridData, win)
        if (t!=None): t.undraw()
        if mode == tile.BLOCKED:
            t = Text(Point(2,-0.5), "placing obsticles")
        elif mode == tile.QUERRYPOINT:
            t = Text(Point(2,-0.5), "placing querry points")
        else:
            print("weird behavior")
        t.draw(win)

        if(len(querryPoints) == 2):
            aStar(gridData, querryPoints[0], querryPoints[1])

    win.close()
