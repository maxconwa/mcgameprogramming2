// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Lab2Actor.generated.h"

UCLASS()
class LABS_API ALab2Actor : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	ALab2Actor();
    
    UPROPERTY(EditAnywhere, BlueprintReadWrite)
        float speed;
    UPROPERTY(EditAnywhere, BlueprintReadWrite)
        FVector EndPosition;

    UPROPERTY(VisibleAnywhere)
        FVector Target;
    
    UPROPERTY(VisibleAnywhere)
        FVector InitialPosition;
    
    UPROPERTY(visibleAnywhere)
        UStaticMeshComponent* theMesh;
    
    UFUNCTION(BlueprintPure)
        static FVector MoveTowards(
            UPARAM(DisplayName = "CurrentLocation") FVector CurrentLocation,
            UPARAM(DisplayName = "Destination") FVector Destination,
            UPARAM(DisplayName = "Max Distance Delta") float maxDistanceDelta);
                  
    
    
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
