// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "MyPawn.generated.h"
//https://docs.unrealengine.com/4.26/en-US/ProgrammingAndScripting/ProgrammingWithCPP/CPPTutorials/PlayerInput/
UCLASS()
class LABS_API AMyPawn : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	AMyPawn();
	FRotator  defaultRotation;
	int score;
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	

	UPROPERTY(VisibleAnywhere, Category = "Pawn")
	UStaticMeshComponent* MeshComp;

	AActor* Arm;
	
	//Input functions
    void Input_XAxis(float AxisValue);
    void Input_YAxis(float AxisValue);
	void zPress();
	void zRelease();
	void Build_Impulse();
    void Apply_Impulse();

	
	

    //Input variables
	UPROPERTY(VisibleAnywhere, Category = "Movement")
    FVector NextMove;
	UPROPERTY(VisibleAnywhere, Category = "Movement")
	int StartingImpulse;
	UPROPERTY(VisibleAnywhere, Category = "Movement")
	int GrowthRate;
	UPROPERTY(VisibleAnywhere, Category = "Movement")
	int NextImpulse;
	UPROPERTY(VisibleAnywhere, Category = "Movement")
	int MaxImpulse;

	UFUNCTION()
	void OnOverlapBegin(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	// declare overlap end function
	UFUNCTION()
	void OnOverlapEnd(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

	UFUNCTION()
	void OnHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent, FVector NormalImpulse, const FHitResult& Hit);
};
