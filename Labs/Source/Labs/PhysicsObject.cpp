// Fill out your copyright notice in the Description page of Project Settings.


#include "PhysicsObject.h"
#include "Components/BoxComponent.h"
#define printRed(text) if (GEngine) GEngine->AddOnScreenDebugMessage(-1,5,FColor::Red, text)

// Sets default values for this component's properties
UPhysicsObject::UPhysicsObject()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	HitBox = CreateDefaultSubobject<UBoxComponent>(TEXT("Box"));

	// ...
}


// Called when the game starts
void UPhysicsObject::BeginPlay()
{
	Super::BeginPlay();

	FScriptDelegate onHitDelegate;
	onHitDelegate.BindUFunction(this, "ComponenentWasHit");
	HitBox->OnComponentHit.Add(onHitDelegate);

	FScriptDelegate beginDelegatee;
	beginDelegatee.BindUFunction(this, "ComponentBeginOverlap");
	HitBox->OnComponentBeginOverlap.Add(beginDelegatee);

	HitBox->SetSimulatePhysics(true);
	HitBox->SetCollisionProfileName(TEXT("BlockAllDynamic"));
	HitBox->SetNotifyRigidBodyCollision(true);

	
	
}


// Called every frame
void UPhysicsObject::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

void UPhysicsObject::ComponenentWasHit(UPrimitiveComponent* HitComponent, AActor* OtherActor,
				UPrimitiveComponent* OtherComponent, FVector NormalizeImpulss,
				const FHitResult& Hit)
{
	printRed(TEXT("I was hit"));
}

void UPhysicsObject::ComponentBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
	UPrimitiveComponent * OtherComponent, int32 OtherBodyIndex, bool bFromSweep,
	const FHitResult &SweepResult)
{
	printRed(TEXT("Overlap begin"));
}
