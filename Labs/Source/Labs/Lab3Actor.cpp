// Fill out your copyright notice in the Description page of Project Settings.


#include "Lab3Actor.h"

#include "MyActorComponent.h"


// Sets default values
ALab3Actor::ALab3Actor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	MeshComp = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));
	SetRootComponent(MeshComp);
	numChildren = 0;
	spawnChildren = true;
	speed = 1;
	motion = true;
	
	static ConstructorHelpers::FObjectFinder<UStaticMesh> CubeVisualAsset(TEXT("/Game/StarterContent/Shapes/Shape_Cube"));

	if(CubeVisualAsset.Succeeded()){
		MeshComp->SetStaticMesh(CubeVisualAsset.Object);
		MeshComp->SetRelativeLocation(FVector(0.0f, 0.0f, 0.0f));
	}

	SetActorLocation(FVector(0,0,25));

	
}

// Called when the game starts or when spawned
void ALab3Actor::BeginPlay()
{
	Super::BeginPlay();
	
	if(numChildren == 1)
	{
		UMyActorComponent* comp = CreateDefaultSubobject<UMyActorComponent>(TEXT("Bouncer"));
		comp->RegisterComponent();
		
		
	}
	
}

// Called every frame
void ALab3Actor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	//SetActorLocation();
	if(motion)
	{
		SetActorRotation(GetActorRotation() +  FRotator(0,1,0) * speed);
		if (numChildren == 0)
		{
			t += DeltaTime;
			FVector location = GetActorLocation();
			FVector newLocation = location + FVector(0,0, sin(t) * 1);
			SetActorLocation(newLocation);
		}
		
	}
	
	if(numChildren > 0 && spawnChildren)
	{
		ALab3Actor* child = GetWorld()->SpawnActor<ALab3Actor>(GetActorLocation(), FRotator(0,0,0));
		child->numChildren = numChildren-1;
		child->SetActorLocation(GetActorLocation() + FVector(250,0,0));
		child->AttachToActor(this, FAttachmentTransformRules::KeepWorldTransform);
		spawnChildren = false;
	}
	
	
	

}

