// Fill out your copyright notice in the Description page of Project Settings.


#include "ClassGameMode.h"
#include "ClassGameState.h"

AClassGameMode::AClassGameMode()
{
	GameStateClass = AClassGameState::StaticClass();
}

void AClassGameMode::DecreaseCoins(int32 amt)
{
	GetGameState<AClassGameState>()->CoinCount -= amt;
}

void AClassGameMode::IncreaseLife(int32 amt)
{
	GetGameState<AClassGameState>()->Life += amt;
}
