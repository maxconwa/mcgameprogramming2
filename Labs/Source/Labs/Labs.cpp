// Copyright Epic Games, Inc. All Rights Reserved.

#include "Labs.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, Labs, "Labs" );
