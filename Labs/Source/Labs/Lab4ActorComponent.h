// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Lab4ActorComponent.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class LABS_API ULab4ActorComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	ULab4ActorComponent();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	AActor* parent;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TArray<AActor *> children;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FTransform componentTransform;
	

	UFUNCTION(BlueprintCallable)
	void setScale(FVector scale);

	UFUNCTION(BlueprintCallable)
	void setRotation(FQuat rot);

	UFUNCTION(BlueprintCallable)
	void setTranslation(FVector location);
	
	
	
		
};
