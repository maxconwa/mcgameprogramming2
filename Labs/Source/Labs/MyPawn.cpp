// Fill out your copyright notice in the Description page of Project Settings.


#include "MyPawn.h"

#include "Collectable.h"
#include "DrawDebugHelpers.h"
#include "EditorViewportClient.h"
#include "Components/SphereComponent.h"

// Sets default values
AMyPawn::AMyPawn()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	AutoPossessPlayer = EAutoReceiveInput::Player0;
	
	MeshComp = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Pawn"));
	MeshComp->SetupAttachment(RootComponent);
	MeshComp->SetSimulatePhysics(true);
	//MeshComp->SetCollisionProfileName(TEXT("Collectable"));
	MeshComp->OnComponentBeginOverlap.AddDynamic(this, &AMyPawn::OnOverlapBegin);
	MeshComp->OnComponentEndOverlap.AddDynamic(this, &AMyPawn::OnOverlapEnd);
	MeshComp->OnComponentHit.AddDynamic(this, &AMyPawn::OnHit);
	static ConstructorHelpers::FObjectFinder<UStaticMesh> CubeVisualAsset(TEXT("/Game/StarterContent/Shapes/Shape_Sphere"));

	if(CubeVisualAsset.Succeeded()){
		MeshComp->SetStaticMesh(CubeVisualAsset.Object);
		MeshComp->SetRelativeLocation(FVector(0.0f, 0.0f, 0.0f));
	}

	
	
	

	StartingImpulse = 500;
	GrowthRate = StartingImpulse;
	NextImpulse = StartingImpulse;
	MaxImpulse  = 5*StartingImpulse;
	defaultRotation = GetActorRotation();
	MeshComp->SetGenerateOverlapEvents(true);
	score = 0;
}

// Called when the game starts or when spawned
void AMyPawn::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AMyPawn::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	
	FlushPersistentDebugLines(GetWorld());
	
	DrawDebugLine(GetWorld() ,
		GetActorLocation(),
		GetActorLocation() + (NextMove * NextImpulse)/500,
		FColor(255,0,0),
		true,
		0,
		0,
		25);
	//lock y rotation
	SetActorRotation(defaultRotation);
}

// Called to bind functionality to input
void AMyPawn::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	InputComponent->BindAction("Impulse", IE_Repeat, this, &AMyPawn::Build_Impulse);
	InputComponent->BindAction("Impulse", IE_Released, this, &AMyPawn::Apply_Impulse);
	InputComponent->BindAction("zAxis", IE_Pressed, this, &AMyPawn::zPress);
	InputComponent->BindAction("zAxis", IE_Released, this, &AMyPawn::zRelease);


	InputComponent->BindAxis("xAxis", this, &AMyPawn::Input_XAxis);
	InputComponent->BindAxis("yAxis", this, &AMyPawn::Input_YAxis);
}


void AMyPawn::Input_XAxis(float AxisValue)
{
    NextMove.X = FMath::Clamp(AxisValue, -1.0f, 1.0f) * 100.0f;
}

void AMyPawn::Input_YAxis(float AxisValue)
{
    NextMove.Y = FMath::Clamp(AxisValue, -1.0f, 1.0f) * 100.0f;
}

void AMyPawn::zPress()
{
	if(GetVelocity().Y != 0)
	{
		NextMove.Z = -100;
	} else
	{
		NextMove.Z = 100;
	}
}
void AMyPawn::zRelease()
{
	NextMove.Z = 0;
}

void AMyPawn::Build_Impulse()
{
	NextImpulse+=GrowthRate;
	if(NextImpulse > MaxImpulse)
	{
		NextImpulse = MaxImpulse;
	}
	
}

void AMyPawn::Apply_Impulse()
{
	MeshComp->AddImpulse(NextMove * NextImpulse);
	NextImpulse = StartingImpulse;
}

void AMyPawn::OnOverlapBegin(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	UE_LOG(LogTemp, Warning, TEXT("Overlap begin called"));
	GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("Overlap Begin From Pawn"));
	score+=1;
	
} 

void AMyPawn::OnOverlapEnd(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, TEXT("Overlap End From Pawn"));
}
void AMyPawn::OnHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent, FVector NormalImpulse, const FHitResult& Hit)
{
	GEngine->AddOnScreenDebugMessage(-1, 1.f, FColor::Green, TEXT("OnHit From Pawn"));
	
}
