// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "LabsGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class LABS_API ALabsGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
