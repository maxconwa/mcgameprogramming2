// Fill out your copyright notice in the Description page of Project Settings.


#include "RotationComponent.h"


#define printRed(text) if (GEngine) GEngine->AddOnScreenDebugMessage(-1,5, FColor::Red, text);
// Sets default values for this component's properties
URotationComponent::URotationComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	if(GetOwner() == nullptr)
	{
		printRed(TEXT("Rotating component doesnt have owner"));
	} else
	{
		componentToRotate = GetOwner()->FindComponentByClass<USceneComponent>();

		componentToRotate->SetMobility(EComponentMobility::Movable);
		
	}

	rotationSpeed = 1.0f;
	rotationAxis = FVector(0.0f, 0.0f, 1.0f);

	
}


// Called when the game starts
void URotationComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...
	if(componentToRotate == nullptr)
	{
		if(GetOwner() == nullptr)
		{
			printRed(TEXT("Rotating component doesnt have owner in begin play"));
		}
		else
		{
			componentToRotate = GetOwner()->FindComponentByClass<USceneComponent>();

			componentToRotate->SetMobility(EComponentMobility::Movable);
		}
	}
	
}


// Called every frame
void URotationComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
	FQuat rotation = FQuat(rotationAxis, FMath::DegreesToRadians(rotationSpeed*DeltaTime));

	componentToRotate->AddWorldRotation(rotation, false);
	// ...
}

