// Fill out your copyright notice in the Description page of Project Settings.


#include "Lab6Pawn.h"

// Sets default values
ALab6Pawn::ALab6Pawn()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	AutoPossessPlayer = EAutoReceiveInput::Player0;
	MeshComp = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Pawn"));
	MeshComp->SetupAttachment(RootComponent);
	MeshComp->SetSimulatePhysics(true);
	
	static ConstructorHelpers::FObjectFinder<UStaticMesh> CubeVisualAsset(TEXT("/Game/StarterContent/Shapes/Shape_Cube"));

	if(CubeVisualAsset.Succeeded()){
		MeshComp->SetStaticMesh(CubeVisualAsset.Object);
		MeshComp->SetRelativeLocation(FVector(0.0f, 0.0f, 0.0f));
	}

}

// Called when the game starts or when spawned
void ALab6Pawn::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ALab6Pawn::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

// Called to bind functionality to input
void ALab6Pawn::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	InputComponent->BindAxis("Yaw", this, &ALab6Pawn::yawAxis);
	InputComponent->BindAxis("Pitch", this, &ALab6Pawn::pitchAxis);
	InputComponent->BindAction("Forward", IE_Repeat, this, &ALab6Pawn::forward);


}

void ALab6Pawn::yawAxis(float axisValue)
{
	//yaw is z
	FQuat rotation = FQuat(FVector(0,0,1) * axisValue, FMath::DegreesToRadians(1));
	AddActorWorldRotation(rotation);
}
void ALab6Pawn::pitchAxis(float axisValue)
{
	//pitch is y
	FQuat rotation = FQuat(FVector(0,1,0) * axisValue, FMath::DegreesToRadians(1));
	AddActorWorldRotation(rotation);

}
void ALab6Pawn::forward()
{
	FVector location = GetActorLocation();
	FVector newLocation = location + (GetActorForwardVector() * 100);
	SetActorLocation(newLocation);
}
