// Fill out your copyright notice in the Description page of Project Settings.


#include "Collectable.h"

#include "EditorTutorial.h"
#include "Components/PrimitiveComponent.h"
#include "Engine/GameEngine.h"


// Sets default values
ACollectable::ACollectable()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	MeshComp = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Pawn"));
	MeshComp->SetupAttachment(RootComponent);
	MeshComp->SetSimulatePhysics(true);
	//MeshComp->SetCollisionProfileName(TEXT("Collectable"));
	MeshComp->OnComponentBeginOverlap.AddDynamic(this, &ACollectable::OnOverlapBegin);
	MeshComp->OnComponentEndOverlap.AddDynamic(this, &ACollectable::OnOverlapEnd);
	MeshComp->OnComponentHit.AddDynamic(this, &ACollectable::OnHit);
	static ConstructorHelpers::FObjectFinder<UStaticMesh> CubeVisualAsset(TEXT("/Game/StarterContent/Shapes/Shape_Torus"));

	if(CubeVisualAsset.Succeeded()){
		MeshComp->SetStaticMesh(CubeVisualAsset.Object);
		MeshComp->SetRelativeLocation(FVector(0.0f, 0.0f, 0.0f));
	}

	MeshComp->SetGenerateOverlapEvents(true);
}

// Called when the game starts or when spawned
void ACollectable::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ACollectable::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ACollectable::OnOverlapBegin(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	UE_LOG(LogTemp, Warning, TEXT("Overlap begin called"));
	GEngine->AddOnScreenDebugMessage(-1, 1.f, FColor::Green, TEXT("Overlap Begin From Collectable"));
	this->Destroy();
} 

void ACollectable::OnOverlapEnd(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	GEngine->AddOnScreenDebugMessage(-1, 1.f, FColor::Red, TEXT("Overlap End From Collectable"));
}

void ACollectable::OnHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent, FVector NormalImpulse, const FHitResult& Hit)
{
	GEngine->AddOnScreenDebugMessage(-1, 1.f, FColor::Green, TEXT("OnHit From Collectable"));
	this->Destroy();
}


