// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Lab3Actor.generated.h"

UCLASS()
class LABS_API ALab3Actor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ALab3Actor();
	UStaticMeshComponent* MeshComp;
	UPROPERTY(EditAnywhere)
	int numChildren;
	UPROPERTY(EditAnywhere)
	int speed;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool motion;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool spawnChildren;
	float t;
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	

};
