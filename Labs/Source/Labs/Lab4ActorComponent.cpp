// Fill out your copyright notice in the Description page of Project Settings.


#include "Lab4ActorComponent.h"


// Sets default values for this component's properties
ULab4ActorComponent::ULab4ActorComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
	// ...
}


// Called when the game starts
void ULab4ActorComponent::BeginPlay()
{
	Super::BeginPlay();
	// ...
	if(parent == nullptr)
	{
		componentTransform = GetOwner()->GetTransform();
	}
	else
	{
		componentTransform = FTransform::Identity;
	}

	
}


// Called every frame
void ULab4ActorComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	if(parent != nullptr)
	{
		FTransform tmp = parent->GetTransform() * componentTransform;
		GetOwner()->SetActorTransform(tmp);

		setScale(tmp.GetScale3D());
		setRotation(tmp.GetRotation());
		setTranslation(tmp.GetLocation());
	}
}



void ULab4ActorComponent::setScale(FVector scale)
{
	componentTransform.SetScale3D(scale);
}
void ULab4ActorComponent::setRotation(FQuat rot)
{
	componentTransform.SetRotation(rot);
}
void ULab4ActorComponent::setTranslation(FVector location)
{
	componentTransform.SetLocation(location);
}