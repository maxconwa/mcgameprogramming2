// Fill out your copyright notice in the Description page of Project Settings.


#include "Lab2Actor.h"

// Sets default values
ALab2Actor::ALab2Actor() : speed(100)
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
    
    theMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));
    theMesh->SetupAttachment(RootComponent);
    
    static ConstructorHelpers::FObjectFinder<UStaticMesh> CubeVisualAsset(TEXT("/Game/StarterContent/Shapes/Shape_Cone"));

	if(CubeVisualAsset.Succeeded()){
		theMesh->SetStaticMesh(CubeVisualAsset.Object);
		theMesh->SetRelativeLocation(FVector(0.0f, 0.0f, 0.0f));
	}
	static ConstructorHelpers::FObjectFinder<UMaterialInterface>
		CubeMaterial(TEXT("/Game/StarterContent/Materials/M_cobblestone_Pebble"));
	if(CubeMaterial.Succeeded()){
		theMesh->SetMaterial(0, CubeMaterial.Object);
	}
}

// Called when the game starts or when spawned
void ALab2Actor::BeginPlay()
{
	Super::BeginPlay();
	InitialPosition = GetActorLocation();
	Target = EndPosition;
	
}

// Called every frame
void ALab2Actor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	if(GetActorLocation().Equals(Target)){
		if(Target.Equals(EndPosition)){
			Target = InitialPosition;
		}
		else{
			Target = EndPosition;
		}
	}
	else{
		FVector nextPos = ALab2Actor::MoveTowards(GetActorLocation(), Target, speed * DeltaTime);
		SetActorLocation(nextPos);
	}

}

FVector ALab2Actor::MoveTowards(FVector currentLocation, FVector Destination, float MaxDistanceDelta){
		FVector towards = Destination - currentLocation;
		float sqDist = towards.SizeSquared();
		float sqMax = MaxDistanceDelta * MaxDistanceDelta;

		if(sqDist < sqMax){
			return Destination;
		} else{
			towards.Normalize();
			return currentLocation + (towards * MaxDistanceDelta);
		}
	}

