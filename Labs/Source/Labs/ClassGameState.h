// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameStateBase.h"
#include "ClassGameState.generated.h"

/**
 * 
 */
UCLASS()
class LABS_API AClassGameState : public AGameStateBase
{
	GENERATED_BODY()
public:
	AClassGameState();
	virtual ~AClassGameState() = default;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 CoinCount;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 Life;
	
};
