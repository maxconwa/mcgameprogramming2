// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "ClassGameMode.generated.h"

/**
 * 
 */
UCLASS()
class LABS_API AClassGameMode : public AGameModeBase
{
	GENERATED_BODY()
public:
	AClassGameMode();
	virtual  ~AClassGameMode() = default;
	
	UFUNCTION(BlueprintCallable, Category = "default")
	virtual void DecreaseCoins(int32 amt);

	UFUNCTION(BlueprintCallable, Category = "default")
	virtual void IncreaseLife(int32 amt);
	
};
